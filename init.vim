set ruler
set number  relativenumber

" line numbers in inactive window
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40<Paste>

:set mouse=a

set ignorecase
set smartcase
set hlsearch

if &compatible
  set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.nvim_plugins/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.nvim_plugins')
  call dein#begin('~/.nvim_plugins')

  call dein#add('~/.nvim_plugins/repos/github.com/Shougo/dein.vim')

  " basic plugins - ctrlP, ack, Nerdtree, commentary
  " call dein#add('ctrlpvim/ctrlp.vim')
  call dein#add('mileszs/ack.vim')
  call dein#add('scrooloose/nerdtree')
  call dein#add('tpope/vim-commentary')
  call dein#add('/usr/local/opt/fzf')
  call dein#add('junegunn/fzf.vim')

  " auto-pairing braces etc and surround in braces
  call dein#add('jiangmiao/auto-pairs')
  call dein#add('tpope/vim-surround')


  " Python features and completion
  call dein#add('davidhalter/jedi-vim')
  " call dein#add('Shougo/deoplete.nvim')
  " call dein#add('zchee/deoplete-jedi')
  call dein#add('neovim/python-client')
  call dein#add('ervandew/supertab')
  call dein#add('nvie/vim-flake8')

  " Cosmetics - colorschemes, airline etc
  call dein#add('rafi/awesome-vim-colorschemes')
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')
  call dein#add('patstockwell/vim-monokai-tasty')
  call dein#add('Nequo/vim-allomancer')
  call dein#add('romainl/Apprentice')
  call dein#add('rakr/vim-two-firewatch')
  call dein#add('altercation/vim-colors-solarized')

  " Git features - fugitive and gitgutter
  call dein#add('tpope/vim-fugitive')
  call dein#add('airblade/vim-gitgutter')

  " Syntax checker - temporarily disabled
  " call dein#add('scrooloose/syntastic')
  call dein#add('w0rp/ale')

  " Snippets
  call dein#add('SirVer/ultisnips')
  call dein#add('honza/vim-snippets')

  " Syntax higlighting for various file types
  call dein#add('sheerun/vim-polyglot')

  " Buffergator - buffers management
  call dein#add('jeetsukumaran/vim-buffergator')

  "ctags management
  call dein#add('ludovicchabant/vim-gutentags')

  "tagbar
  call dein#add('majutsushi/tagbar')

  "Illuminating matching words under the cursor
  call dein#add('RRethy/vim-illuminate')
  " call dein#add('numirias/semshi')

  "editorconfig
  call dein#add('editorconfig/editorconfig-vim')


  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
" syntax enable

syntax enable
colorscheme onedark


" ctrlp options
" buf ctrlp under ctrl + b, tag ctrlp under ctrl + x
:nmap <C-b> :CtrlPBuffer<cr>
" :nmap <C-x> :CtrlPTag<cr>
:nmap <C-x> :Tags<cr>
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
  \ --ignore .git
  \ --ignore .svn
  \ --ignore .hg
  \ --ignore .DS_Store
  \ --ignore "**/*.pyc"
  \ -g ""'

" ctrlp ignore
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/__pycache__/*,tags,*/.mypy_cache/*,*pyc

" FZF options
:nmap <C-x> :Tags<cr>
:nmap <F12> :BTags<cr>
:nmap <ALT-p> :FZF<cr>

" NERDTree options
let NERDTreeIgnore = ['\.pyc$']
:nmap <C-n> :NERDTreeToggle<cr>

" Gitgutter configuration
set updatetime=100
:nmap <C-Down> :GitGutterNextHunk<cr>
:nmap <C-Up> :GitGutterPrevHunk<cr>

" Tagbar options
:nmap <F8> :Tagbar<cr>


" jedi options
autocmd FileType python setlocal completeopt-=preview
let g:jedi#completions_enabled = 1
let g:jedi#use_tabs_not_buffers = 1
let g:jedi#popup_on_dot = 1


" deoplete
" let g:deoplete#enable_at_startup = 1

" vim-airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#ale#enabled = 1

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


" Snippets settings
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Remove trailing whitespaces on save
autocmd BufWritePre * :%s/\s\+$//e

hi link illuminatedWord Visual

autocmd VimEnter * ALEDisable

:nmap <S-Down> :ALENext<cr>
:nmap <S-Up> :ALEPrevious<cr>

:nmap <C-p> :GFiles<cr>
:nmap <C-b> :Buffers<cr>

let g:ale_fixers = {
\  '*': ['remove_trailing_lines', 'trim_whitespace'],
\  'python': ['isort'],
\}

" Normal mode in terminal with escape key
if has('nvim')
	" map escape to normal mode in terminal
	tnoremap <Esc> <C-\><C-n>
	" map Alt-[ to escape, to be able to send escape key to terminal
	" (previous mapping took that away)
	tnoremap <A-[> <Esc>
	" use double ctrl-w to swith to other split in terminal (just like in
	" normal mode)
	tnoremap <C-w><C-w> <C-\><C-n><C-w><C-w>
endif
